package com.devcamp.rest_api;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class apiController {
    static Staff staff1 = new Staff(1, "Tôn Nữ Linh Giang", 28);
    static Staff staff2 = new Staff(2, "Trần Hồng Ngọc", 23);
    static Staff staff3 = new Staff(3, "Tôn Châu Giang", 29);
    static Staff staff4 = new Staff(4, "Tôn Quang Huy", 34);
    static Staff staff5 = new Staff(5, "Trần Ngọc Linh", 20);
    static Staff staff6 = new Staff(6, "Lê Thanh Ngân", 19);
    static Staff staff7 = new Staff(7, "Nguyễn Đăng Thông", 30);
    static Staff staff8 = new Staff(8, "Hoàng Công Linh", 31);
    static Staff staff9 = new Staff(9, "Phạm Trung Linh", 36);
    static Department department1 = new Department(1, "Khoa Du Lịch", "TP. Thủ Đức", stafflist1());
    static Department department2 = new Department(2, "Khoa Dược", "Quận 1", stafflist2());
    static Department department3 = new Department(3, "Khoa Giải phẫu", "Quận 3", stafflist3());

    public static ArrayList<Staff> stafflist1() {
        ArrayList<Staff> stafflist1 = new ArrayList<>();
        stafflist1.add(staff1);
        stafflist1.add(staff2);
        stafflist1.add(staff3);
        return stafflist1;
    }

    public static ArrayList<Staff> stafflist2() {
        ArrayList<Staff> stafflist2 = new ArrayList<>();
        stafflist2.add(staff4);
        stafflist2.add(staff5);
        stafflist2.add(staff6);
        return stafflist2;
    }

    public static ArrayList<Staff> stafflist3() {
        ArrayList<Staff> stafflist3 = new ArrayList<>();
        stafflist3.add(staff7);
        stafflist3.add(staff8);
        stafflist3.add(staff9);
        return stafflist3;
    }

    // api trả về danh sách tất cả các phòng
    @GetMapping("/AllDepartments")
    public static ArrayList<Department> Department() {
        ArrayList<Department> Department = new ArrayList<>();
        Department.add(department1);
        Department.add(department2);
        Department.add(department3);
        return Department;
    }

    // api trả về department id tương ứng
    @GetMapping("/DepartmentID")
    public Department department_id(@RequestParam(value = "id", defaultValue = "1") int id) {
        ArrayList<Department> Department = Department();
        Department DepartmentFound = new Department();
        for (Department department : Department) {
            if (department.getId() == id) {
                DepartmentFound = department;
            }
        }
        return DepartmentFound;
    }

    // api trả về danh sách tất cả nhân viên
    @GetMapping("/AllStaffs")
    public static ArrayList<Staff> staffs() {
        ArrayList<Staff> staffs = new ArrayList<>();
        staffs.add(staff1);
        staffs.add(staff2);
        staffs.add(staff3);
        staffs.add(staff4);
        staffs.add(staff5);
        staffs.add(staff6);
        staffs.add(staff7);
        staffs.add(staff8);
        staffs.add(staff9);
        return staffs;
    }

    // api trả về nhân viên có số tuổi đã truyền vào
    @GetMapping("/StaffAge")
    public ArrayList<Staff> staff_age(@RequestParam(value = "age", defaultValue = "1") int age) {
        ArrayList<Staff> staff = staffs();
        ArrayList<Staff> staffFound = new ArrayList<>();
        for (Staff staffage : staff) {
            if (staffage.getAge() > age) {
                staffFound.add(staffage);
            }
        }
        return staffFound;
    }

    // api trả về về danh sách phòng ban có độ tuổi trung bình lớn hơn averageAge
    @GetMapping(value = "/AverageAge")
    public ArrayList<Department> averageAge(@RequestParam(value = "averageage", defaultValue = "1") float age) {
        ArrayList<Department> Department = Department();
        ArrayList<Department> DepartmentFound = new ArrayList<>();
        for (Department departmentage : Department) {
            if (departmentage.getAverageAge() > age) {
                DepartmentFound.add(departmentage);
            }
        }
        return DepartmentFound;

    }

}
